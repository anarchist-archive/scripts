#!/usr/bin/env python3
import logging
import sys
from argparse import ArgumentParser
from pathlib import Path
from tempfile import NamedTemporaryFile

import dateparser
import requests
from bs4 import BeautifulSoup
from slugify import slugify

from common import ROOT, import_command, pandoc_command

log = logging.getLogger(__file__)
logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)


def main() -> None:
    args = arg_parser().parse_args()

    resp = requests.get(args.url)
    resp.raise_for_status()

    soup = BeautifulSoup(resp.text, features="html5lib")

    title_node = soup.select(".entry-title")[0]
    title = title_node.string

    if args.author:
        title_only = args.title
    else:
        if title:
            title_only = title
        else:
            # TODO actually handle subtitle
            title_split = str(title_node).split("<br>")
            title_only = title_split[0]

    if not args.author:
        author = str(soup.select(".entry-author a")[0].string)
        assert author
        author = author.capitalize()
        if author == "Gäst":
            author = "Anonymous"
    else:
        author = args.author

    slug = slugify(author + " " + title_only, allow_unicode=True)

    date = dateparser.parse(soup.select("span.entry-date > a")[0].string)
    # `dateparser.parse` returns None on failure instead of raising an exception
    if date is None:
        raise Exception("Failed to parse date")

    with NamedTemporaryFile("w", suffix=".json", delete=False) as f:
        log.info(f"Using temp file: {f.name}")
        pandoc_command(
            str(soup.select(".entry-content")[0]),
            f.name,
            pandoc_filters=[ROOT / "anarkism_info_filter.py"],
        )
        import_command(
            pandoc_file=f.name,
            lang=args.lang,
            slug=slug,
            title=title,
            contributors=[
                f"aut:{author}",
                "pbl:false:anarkism.info",
            ],
            publication_date=date.date(),
            url=args.url,
        )


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name, description="Parse Data from C4SS")
    parser.add_argument("url", help="URL to download")
    parser.add_argument("-l", "--lang", default="se", help="language tag")
    parser.add_argument("-a", "--author", help="override author")
    parser.add_argument("-t", "--title", help="override title")
    return parser


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("")
        sys.exit(1)
