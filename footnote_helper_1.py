#!/usr/bin/env python3
import re
import sys
from argparse import ArgumentParser
from collections import defaultdict
from pathlib import Path


def main():
    args = arg_parser().parse_args()
    fn_regex = re.compile('<fn id="(\\d+-\\d+)"')
    footnote_regex = re.compile('<footnote id="(\\d+-\\d+)"')

    with open(args.file) as f:
        data = f.read().split("\n")

    line_count = len(data)

    # line -> ids
    fn_ids = defaultdict(list)
    for i, line in enumerate(data):
        matches = list(fn_regex.finditer(line))
        if not matches:
            continue
        for match in matches:
            fn_ids[i].append(match.group(1))

    # id -> text
    footnote_lines = set()
    footnote_ids = defaultdict(list)
    for i, line in enumerate(data):
        matches = list(footnote_regex.finditer(line))
        if not matches:
            continue
        footnote_lines.add(i)
        for match in matches:
            footnote_ids[match.group(1)] = line

    assert set(fn_ids.keys()) & set(footnote_ids.keys()) == set()

    out = []
    for i, line in enumerate(data):
        if i in footnote_lines:
            continue
        out.append(line)
        if i not in fn_ids:
            continue
        for note_id in fn_ids[i]:
            out.append(footnote_ids[note_id])

    assert len(out) == line_count

    with open(args.file, "w") as f:
        f.write("\n".join(out))


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name, description="Realign footnotes in XML")
    parser.add_argument("file")
    return parser


if __name__ == "__main__":
    main()
