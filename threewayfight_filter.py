#!/usr/bin/env python3
from panflute import Div, run_filter


def process(elem, doc):
    if isinstance(elem, Div):
        if "post-share-buttons" in div.classes:
            div.blocks.clear()


def main(doc=None):
    return run_filter(process, doc=doc)


if __name__ == "__main__":
    main()
