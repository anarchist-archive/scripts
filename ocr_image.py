#!/usr/bin/env python3
import sys
from argparse import ArgumentParser
from pathlib import Path

import pytesseract
from PIL import Image


def main() -> None:
    args = arg_parser().parse_args()

    for file in args.file:
        print(f"Processing file: {file}", file=sys.stderr)
        print(pytesseract.image_to_string(Image.open(file)))


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name, description="OCR an image")
    parser.add_argument("file", metavar="PATH", nargs="+", help="File to OCR")
    return parser


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("")
        sys.exit(1)
