#!/usr/bin/env python3
import sys
import xml.etree.ElementTree as ET
from argparse import ArgumentParser, FileType
from pathlib import Path


def main() -> None:
    args = arg_parser().parse_args()
    try:
        _ = ET.fromstring(args.file.read())
    except ET.ParseError as e:
        print(e)
        sys.exit(1)
    print("Ok!")


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name)
    parser.add_argument("file", type=FileType("r"))
    return parser


if __name__ == "__main__":
    main()
