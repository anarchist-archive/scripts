#!/usr/bin/env python3
import logging
import sys
from argparse import ArgumentParser
from pathlib import Path
from tempfile import NamedTemporaryFile

import requests
from bs4 import BeautifulSoup
from slugify import slugify

from common import ROOT, import_command, pandoc_command

log = logging.getLogger(__file__)
logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)


def main() -> None:
    args = arg_parser().parse_args()

    if args.url.startswith("file://"):
        with open(args.url[7:]) as f:
            data = f.read()
    else:
        resp = requests.get(args.url)
        resp.raise_for_status()
        data = resp.text

    soup = BeautifulSoup(data, features="html5lib")
    html = str(soup.select(args.selector)[0])

    with NamedTemporaryFile("w", suffix=".json", delete=False) as f:
        log.info(f"Using temp file: {f.name}")
        pandoc_command(html, f.name)
        import_command(
            pandoc_file=f.name,
            lang=args.lang,
            slug=slugify(args.author + " " + args.title),
            title=args.title,
            contributors=[
                f"aut:{args.author}",
            ],
            url=args.url,
            publication_date=args.date,
        )


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name, description="Parse Data from C4SS")
    parser.add_argument("url", help="URL to download")
    parser.add_argument("-l", "--lang", required=True, help="language tag")
    parser.add_argument("-a", "--author", required=True, help="author name")
    parser.add_argument("-t", "--title", required=True, help="author name")
    parser.add_argument("-d", "--date", help="publication date")
    parser.add_argument("-s", "--selector", required=True, help="css selector for main document")
    return parser


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("")
        sys.exit(1)
