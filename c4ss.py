#!/usr/bin/env python3
import logging
import sys
from argparse import ArgumentParser
from datetime import date, datetime
from pathlib import Path
from tempfile import NamedTemporaryFile

import requests
from bs4 import BeautifulSoup
from slugify import slugify

from common import ROOT, import_command, pandoc_command

log = logging.getLogger(__file__)
logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)


def main() -> None:
    args = arg_parser().parse_args()

    resp = requests.get(args.url)
    resp.raise_for_status()

    soup = BeautifulSoup(resp.text, features="html5lib")
    html = str(soup.select(".blog-content-post-text")[0])

    author = soup.select(".byline > a")[0].string
    assert author

    title = soup.select(".blog-content-post-title")[0].string
    assert title

    slug = slugify(author.lower() + " " + title.lower())
    date = parse_date(soup.select(".blog-content-meta > .date")[0].string)

    with NamedTemporaryFile("w", suffix=".json", delete=False) as f:
        log.info(f"Using temp file: {f.name}")
        pandoc_command(
            html,
            f.name,
            pandoc_filters=[ROOT / "c4ss_filter.py"],
        )
        import_command(
            pandoc_file=f.name,
            lang=args.lang,
            slug=slug,
            title=title,
            contributors=[
                f"aut:{author}",
                "pbl:false:C4SS",
            ],
            publication_date=date,
            url=args.url,
        )


def parse_date(date_str: date) -> date:
    date_str = (
        date_str.strip().replace("st,", "").replace("nd,", "").replace("rd,", "").replace("th,", "")
    )
    return datetime.strptime(date_str, "%B %d %Y").date()


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name, description="Parse Data from C4SS")
    parser.add_argument("url", help="URL to download")
    parser.add_argument("-l", "--lang", default="en", help="language tag")
    return parser


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("")
        sys.exit(1)
