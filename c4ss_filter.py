#!/usr/bin/env python3
from panflute import Header, Strong, run_filter


def process(elem, doc):
    if isinstance(elem, Header):
        # increase the header level since they default to 3
        elem.level -= 2

        # strip strongs since the whole header is wrapped in a strong most times
        if len(elem.content) == 1 and isinstance(elem.content[0], Strong):
            elem.content = elem.content[0].content


def main(doc=None):
    return run_filter(process, doc=doc)


if __name__ == "__main__":
    main()
