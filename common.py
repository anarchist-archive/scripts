import logging
import shlex
import subprocess
from datetime import date, datetime
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import List, Optional, Tuple, Union

log = logging.getLogger(__file__)

ROOT = Path(__file__).parent
ANARCHIVE_DIR = ROOT.parent / "anarchive"


def run_command(args: List[str], cwd: Optional[Path] = None) -> None:
    cmd_str = " \\\n  ".join([shlex.quote(x) for x in args])
    log.info(f"Running commaind:\n\n{cmd_str}\n")

    subprocess.check_call(args, cwd=cwd)


def import_command(
    pandoc_file: Path,
    lang: str,
    slug: str,
    title: Optional[str] = None,
    contributors: Optional[List[str]] = None,
    publication_date: Union[datetime, date, Tuple[int, int], int, None] = None,
    url: Optional[str] = None,
) -> None:
    log.info("Running import")

    args = [
        "cargo",
        "run",
        "--",
        "-c",
        "anarchive.toml",
        "import",
        "pandoc",
        str(pandoc_file),
        "--language",
        lang,
        "--slug",
        slug,
    ]

    if title:
        args.extend(["--title", title])

    if contributors:
        for contributor in contributors:
            args.extend(["--contributor", contributor])

    if publication_date is not None:
        if isinstance(publication_date, (datetime, date)):
            date_str = publication_date.isoformat()
        elif isinstance(publication_date, tuple):
            date_str = "-".join(str(x) for x in publication_date)
        else:
            date_str = str(publication_date)

        args.extend(["--publication-date", date_str])

    if url:
        args.extend(["--url", url])

    run_command(args, cwd=ANARCHIVE_DIR)
    log.info("Done running import")


def pandoc_command(
    data: str,
    target: Path,
    pandoc_filters: Optional[List[Path]] = None,
) -> None:
    log.info("Running pandoc")

    with NamedTemporaryFile("w", suffix=".html") as f:
        f.write(data)
        f.flush()

        args = [
            "pandoc",
            f.name,
            "-o",
            str(target),
        ]

        if pandoc_filters:
            for fil in pandoc_filters:
                args.extend(["--filter", str(fil)])

        run_command(args)
    log.info("Done running pandoc")
