#!/usr/bin/env python3
import logging
import sys
from argparse import ArgumentParser
from datetime import date, datetime
from pathlib import Path
from tempfile import NamedTemporaryFile

import requests
from bs4 import BeautifulSoup
from slugify import slugify

from common import ROOT, import_command, pandoc_command

log = logging.getLogger(__file__)
logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)


def main() -> None:
    args = arg_parser().parse_args()

    resp = requests.get(args.url)
    resp.raise_for_status()

    soup = BeautifulSoup(resp.text, features="html5lib")
    html = str(soup.select(".post-body")[0])

    title = soup.select(".post-title > a")[0].string
    assert title

    pbl = "Three-Way Fight"

    slug = slugify((args.author or pbl).lower() + " " + title.lower())
    date = parse_date(soup.select(".post-metanbt .published")[0].string)

    with NamedTemporaryFile("w", suffix=".json", delete=False) as f:
        log.info(f"Using temp file: {f.name}")
        pandoc_command(
            html,
            f.name,
            pandoc_filters=[ROOT / "threewayfight_filter.py"],
        )
        import_command(
            pandoc_file=f.name,
            lang="en",
            slug=slug,
            title=title,
            contributors=[
                f"pbl:false:{pbl}",
            ],
            publication_date=date,
            url=args.url,
        )


def parse_date(date_str: date) -> date:
    date_str = " ".join(date_str.split(", ")[1:])
    return datetime.strptime(date_str, "%B %d %Y").date()


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name, description="Parse Data from C4SS")
    parser.add_argument("-a", "--author", required=True, help="author name")
    parser.add_argument("url", help="URL to download")
    return parser


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("")
        sys.exit(1)
