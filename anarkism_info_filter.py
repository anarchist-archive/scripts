#!/usr/bin/env python3
from panflute import HorizontalRule, Para, Str, run_filter


def process(elem, doc):
    if not isinstance(elem, Para):
        return

    contents = elem.content
    if len(contents) != 1:
        return

    inner = contents[0]
    if not isinstance(inner, Str):
        return

    if inner.text == "***":
        return HorizontalRule


def main(doc=None):
    return run_filter(process, doc=doc)


if __name__ == "__main__":
    main()
