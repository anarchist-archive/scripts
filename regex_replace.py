#!/usr/bin/env python3
import re
from argparse import ArgumentParser
from pathlib import Path


def main() -> None:
    args = arg_parser().parse_args()

    with open(args.file) as f:
        text = f.read()

    text = re.sub(args.regex, args.replacement, text, flags=re.MULTILINE)

    with open(args.file, "w") as f:
        f.write(text)


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name, description="RegEx replacements")
    parser.add_argument("regex")
    parser.add_argument("replacement")
    parser.add_argument("file")
    return parser


if __name__ == "__main__":
    main()
