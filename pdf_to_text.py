#!/usr/bin/env python3
import logging
import math
import sys
from argparse import ArgumentError, ArgumentParser
from dataclasses import dataclass
from pathlib import Path
from typing import Generator, Tuple

import pdf2image
import pytesseract
from pypdf import PdfReader

log = logging.getLogger(__file__)
logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)


class CropError(ValueError):
    pass


@dataclass(frozen=True)
class CropBox:
    top_left_x: int
    top_left_y: int
    bottom_right_x: int
    bottom_right_y: int

    def get_tuple(self, height: int, width: int, x_offset: int = 0) -> Tuple[int, int, int, int]:
        bottom_right_x = self.bottom_right_x
        if self.bottom_right_x > width:
            log.warning(f"Width ({width}) was less than crop box height")
            bottom_right_x = width

        bottom_right_y = self.bottom_right_y
        if bottom_right_y > height:
            log.warning(f"Height ({height}) was less than crop box height")
            bottom_right_y = height
        return (
            self.top_left_x + x_offset,
            self.top_left_y,
            bottom_right_x + x_offset,
            bottom_right_y,
        )


def main() -> None:
    parser = arg_parser()
    args = parser.parse_args()

    if args.extract and args.crop_box is not None:
        parser.error("Argument '--crop-box' cannot be used with '--extract'")

    if not args.extract:
        if args.imposed:
            ocr_pdf_imposed(args.file, args.crop_box)
        else:
            ocr_pdf(args.file, args.crop_box)
    else:
        if args.imposed:
            raise Exception("Cannot extract text from an imposed PDF")
        else:
            extract_pdf(args.file)


def ocr_pdf(file: str, crop_box: CropBox | None) -> None:
    text = ""

    for page_idx, img in enumerate(pdf2image.convert_from_path(file)):
        log.info(f"Processing page {page_idx}")

        if crop_box is not None:
            img = img.crop(crop_box.get_tuple(img.height, img.width))

        text += pytesseract.image_to_string(img)
        text += "\n"

    print(text.strip())


def ocr_pdf_imposed(file: str, crop_box: CropBox | None) -> None:
    pdf = PdfReader(file)
    pages = [None] * len(pdf.pages) * 2  # using a list and not dict to catch bugs via IndexError

    for (left_idx, right_idx), img in zip(
        imposed_for(len(pdf.pages)), pdf2image.convert_from_path(file)
    ):
        log.info(f"Processing pages ({left_idx}, {right_idx})")
        middle_px = math.floor(img.width / 2.0)

        if crop_box is None:
            left_crop = (0, 0, middle_px, img.height)
            right_crop = (middle_px + 1, 0, img.width, img.height)
        else:
            left_crop = crop_box.get_tuple(middle_px, img.height)
            right_crop = crop_box.get_tuple(img.width - middle_px, img.height, middle_px + 1)

        pages[left_idx] = pytesseract.image_to_string(img.crop(left_crop))
        pages[right_idx] = pytesseract.image_to_string(img.crop(right_crop))

    print("\n".join(pages))


def extract_pdf(file: str) -> None:
    text = ""
    pdf = PdfReader(file)
    for page_idx, page in enumerate(pdf.pages):
        log.info(f"Processing page {page_idx}")
        text += page.extract_text().replace("\n", " ")
        text += "\n"

    print(text)


def imposed_for(pages_imposed: int) -> Generator[Tuple[int, int], None, None]:
    right_count = 0
    left_count = (pages_imposed * 2) - 1

    for page_idx in range(0, pages_imposed):
        if page_idx % 2 == 0:
            yield (left_count, right_count)
        else:
            yield (right_count, left_count)
        right_count += 1
        left_count -= 1


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(Path(__file__).name, description="OCR a PDF")
    parser.add_argument("file", metavar="PATH", help="PDF to OCR")

    imposed_group = parser.add_mutually_exclusive_group()
    imposed_group.add_argument("-i", "--imposed", action="store_true", help="Treat PDF as imposed")
    imposed_group.add_argument(
        "--extract",
        action="store_true",
        help="Extract text from the PDF itself instead of using OCR",
    )

    parser.add_argument(
        "--crop-box",
        type=crop_box,
        help="A crop to apply to pages before running OCR. 4-Tuple of: top_left_x, top_left_y, bottom_right_x, bottom_right_y",
    )

    return parser


def crop_box(string: str) -> CropBox:
    split = string.split(",")
    if len(split) != 4:
        raise ValueError(
            "Must be a CSV 4-tuple of non-negative integers in the format: "
            "top_left_x, top_left_y, bottom_right_x, bottom_right_y"
        )

    split = [int(x) for x in split]

    for num in split:
        if num < 0:
            raise ValueError(f"Negative integer: {num}")

    if split[0] >= split[2]:
        raise ValueError("bottom_right_x must be strictly greater than top_left_x")
    if split[1] >= split[3]:
        raise ValueError("bottom_right_y must be strictly greater than top_left_y")

    return CropBox(*split)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("")
        sys.exit(1)
